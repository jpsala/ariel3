import { inject } from 'aurelia-framework';
import AuthorizeStep from './authorize-step';
import AuthService from './services/auth';
import { Config } from './config.js';
@inject(AuthService, Config)

export class App {
  constructor(auth, config) {
    console.clear();
    console.info('app.constructor');
    this.config = config;
    this.auth = auth;
    this.auth.configureAuth();
  }
  configureRouter(config, router) {
    config.title = 'Gestión';
    config.addPipelineStep('authorize', AuthorizeStep);
    config.map([
      { route: 'home', name: 'home',      moduleId: './home', auth: true,  nav: true, title: 'Home' },
      { route: 'clientes', name: 'clientes',      moduleId: './clientes',  auth: true,  nav: true, title: 'Clientes' },
      { route: 'login', name: 'login', moduleId: './login', title: 'Login', auth: false },
      { route: '', redirect: 'home' }
    ])      
    this.router = router;
  }
}
