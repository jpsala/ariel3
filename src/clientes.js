import { inject } from 'aurelia-framework';
// import $ from 'webpack-zepto';
import { ClientesService } from './services/clientes';
@inject(ClientesService)
export class Clientes {

  constructor(clientesService) {
    this.clientesService = clientesService;
  }

  async activate() {
    this.clientes = await this.clientesService.getClientes();
    // console.log(this.clientes);
  }

}
