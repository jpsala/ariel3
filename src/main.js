// we want font-awesome to load as soon as possible to show the fa-spinner
import '../styles/styles.css';
// import '../node_modules/blaze/dist/blaze.min.css';
// comment out if you don't want a Promise polyfill (remove also from webpack.common.js)
import * as Bluebird from 'bluebird';
Bluebird.config({ warnings: false });

export async function configure(aurelia) {
  aurelia.use
    .standardConfiguration()
    .developmentLogging();

  await aurelia.start();
  aurelia.setRoot('app');

  // if you would like your website to work offline (Service Worker), 
  // install and enable the @easy-webpack/config-offline package in webpack.config.js and uncomment the following code:
  /*
  const offline = await System.import('offline-plugin/runtime');
  offline.install();
  */
}

//jquery:451
//jquery+spectre:481
//jquery+spectre+zepto:518
//blaze+zepto:559