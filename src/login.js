import {inject} from 'aurelia-framework';
import AuthService from 'services/auth';
import {EventAggregator} from 'aurelia-event-aggregator';
import {Router} from 'aurelia-router';
// import alertify from '../node_modules/alertify.js/dist/js/alertify';

@inject(AuthService, Router, EventAggregator)
export class Login {
  username = '';
  password = '';

  constructor(auth, router, ea) {
    // alertify.logPosition('top-right');
    this.auth = auth;
    this.router = router;
    this.ea = ea;
  }

  submit() {
    this.auth.login(this.username, this.password);
  }
}
