import {HttpClient, json} from 'aurelia-fetch-client';
import {inject} from 'aurelia-framework';
import AuthService from '../services/auth';
@inject(HttpClient, AuthService)
export class ClientesService {
  constructor(http, authService) {
    this.http = http;
    this.authService = authService;
  }

  async getClientes(query, limit = 10, conSaldo = false) {
    return await this
      .http
      .fetch('/clientes', {
        method: 'post',
        body: json({query: query, limit: limit, con_saldo: conSaldo})
      }).then((r)=>{
        return r.data.map(c=>new Cliente(c));
      });
  }

}
export class Cliente {
  constructor(cliente) {
    Object.assign(this, cliente);
  }
}
