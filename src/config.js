export class Config {
  local = document.location.hostname === 'localhost';

  getUrlBase() {
    // console.log(document.location.hostname);
    // return 'http://' + '192.168.2.254' + '/iae/';
    return 'http://' + document.location.hostname + '/ariel.yii/';
  };

  getUrlApi() {
    return this.getUrlBase() + '?r=api';
  }

}
