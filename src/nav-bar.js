import { Aurelia, inject, bindable } from 'aurelia-framework';
import { Router } from 'aurelia-router';
@inject(Router, Aurelia, Element)
export class NavBar {
  @bindable auth;
  constructor(router, aurelia, el) {
    this.router = router;
    this.aurelia = aurelia;
    this.el = el;
  }

  attached(){
  	let el;
  	if(el = document.getElementById("mydiv"))
  		el.style.width = el.scrollWidth + "px";
  }

}
